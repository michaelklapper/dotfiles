++++++++++++++++++++++++
Dotfiles
++++++++++++++++++++++++

:Author: Michael Klapper <development@morphodo.com>
:Description: This repository includes all of my custom dotfiles. 
:Homepage: http://www.morphodo.com
:License: MIT License 
:Tags: Home directory, configuration

What does this script do
================
1. Backup dotfiles that are already available in your home directory into the directory ``~/dotfiles.bak``
2. Symlink all dotfiles out of ``~/dotfiles`` into your home directory 

Installation
================

::
	git clone git://github.com/michaelklapper/dotfiles ~/dotfiles
	cd ~/dotfiles
	./make.sh
