#!/bin/bash -e

DIR=~/dotfiles
DIR_BACKUP=~/dotfiles.$(date "+%Y%m%d-%H%M")
FILES=`ls -1A ~/dotfiles | grep -vE 'make.sh|.git|.gitignore|.make.sh.swp|README.rst|LICENSE'`
echo $DIR_BACKUP

for file in $FILES; do
    if [[ -f ~/$file || -d ~/$file ]]; then
        test -d $DIR_BACKUP || mkdir $DIR_BACKUP
        mv ~/$file $DIR_BACKUP
    fi
    test -L ~/$file || ln -s $DIR/$file ~/$file
done

if [ -d $DIR_BACKUP ]; then
    echo "Backup of following files done:"
    ls -1A $DIR_BACKUP
fi

