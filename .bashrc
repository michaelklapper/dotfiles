
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

# rvm
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

if [ -d ~/.bashrc.d ]; then
    for i in ~/.bashrc.d/*; do
        if [ -f $i ]; then
            source $i
        fi
    done
fi

if [ -d ~/.bash_aliases.d ]; then
    for i in ~/.bash_aliases.d/*; do
        . $i
    done
fi

################################################
# bash functions

function myip {
  res=$(curl -s checkip.dyndns.org | grep -Eo '[0-9\.]+')
  echo "$res"
}

